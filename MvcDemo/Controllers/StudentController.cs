﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Collections;
using System.IO;
using MvcDemo.Models;
using Newtonsoft.Json;

namespace MvcDemo.Controllers
{
    public class StudentController : Controller
    {
        //
        // GET: /Student/
        public ActionResult Index()
        {
            return View();
        }

        /// <summary> 获取所有学生信息，并转换为Json </summary>
        /// <returns>学生信息集</returns>
        public JsonResult GetALLStu(string sXH)
        {
            List<Student> datas = new List<Student>();
            Student t = null;
            DataSet ds = StudentDao.GetAllStu(sXH);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                t = new Student();
                t.ID = dr["ID"].ToString();
                t.NO = dr["NO"].ToString();
                t.Name = dr["Name"].ToString();
                t.Sex = dr["Sex"].ToString();
                datas.Add(t);
            }
            return Json(datas);
        }

       /// <summary>
       /// 删除学生信息
       /// </summary>
        /// <returns>success：成功 fail:失败</returns>
        public ContentResult DeleteStuById(string ID) 
        {
            string sRet = StudentDao.DeleteStuById(ID);
            return Content(sRet);
        }

        /// <summary>
        /// 添加学生信息
        /// </summary>
        /// <returns>success：成功 fail:失败</returns>
        public ContentResult AddStu(string data)
        {
            //将从View传来的Json串转换为对象
            Student stu = JsonConvert.DeserializeObject<Student>(data);
            string sRet = StudentDao.AddStu(stu);
            return Content(sRet);
        }

        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <returns>success：成功 fail:失败</returns>
        public ContentResult EditStu(string data)
        {
            //将从View传来的Json串转换为对象
            Student stu = JsonConvert.DeserializeObject<Student>(data);
            string sRet = StudentDao.EditStu(stu);
            return Content(sRet);
        }
    }
}

﻿/*
 * 操作数据库
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MvcDemo.Models
{
    public class DataAccess
    {
        public static string connStr = ConfigurationManager.AppSettings["ConnStr"]; //从配置文件读取数据库连接字符串
        
        /// <summary>
        /// 查询数据库数据
        /// </summary>
        /// <returns>数据集</returns>
        public static DataSet QuerySql(string sSql)
        { 
            SqlConnection conn=new SqlConnection(connStr);
            conn.Open();
            SqlDataAdapter mAdapter=new SqlDataAdapter(sSql,conn);
            DataSet ds=new DataSet();
            mAdapter.Fill(ds);
            conn.Close();
            return ds;
        }

        /// <summary>
        /// 执行增、删、改语句
        /// </summary>
        /// <returns>受影响的行数</returns>
        public static int ExecSql(string sSql)
        { 
            SqlConnection conn=new SqlConnection(connStr);
            conn.Open();
            SqlCommand comm=new SqlCommand(sSql,conn);
            int sRet=comm.ExecuteNonQuery();
            conn.Close();
            return sRet;
        }
    }
}
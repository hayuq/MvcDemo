﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace MvcDemo.Models
{
    public class StudentDao
    {
        /// <summary>
        /// 查询所有学生信息
        /// </summary>
        /// <returns>数据集</returns>
        public static DataSet GetAllStu() 
        {
            string sql = "select * from tblStu";
            return DataAccess.QuerySql(sql);
        }

        /// <summary>
        /// 根据学号查询学生信息
        /// </summary>
        /// <param name="sXH">学号</param>
        /// <returns>数据集</returns>
        public static DataSet GetAllStu(string sXH)
        {
            string sql = "select * from tblStu";
            if (!string.IsNullOrEmpty(sXH))
                sql = sql + " where NO=" + sXH;
            return DataAccess.QuerySql(sql);
        }

        /// <summary>
        /// 根据学号删除学生信息
        /// </summary>
        /// <param name="sid">学号</param>
        /// <returns>成功：success 失败：fail</returns>
        public static string DeleteStuById(string sid)
        {
            string sql = "delete tblStu where ID=" + sid;
            if (DataAccess.ExecSql(sql) > 0)
                return "success";
            else return "fail";
        }

        /// <summary>
        /// 添加学生信息
        /// </summary>
        /// <param name="t">学生对象</param>
        /// <returns>成功：success 失败：fail exist：已经存在</returns>
        public static string AddStu(Student t) 
        {
            string sSql = "select ID from tblStu where NO='" + t.NO + "'";
            DataSet ds = DataAccess.QuerySql(sSql);
            if (ds.Tables[0].Rows.Count > 0)
                return "exist";
            string sql = "insert tblStu(NO,Name,Sex) values('"+t.NO+"','"+t.Name+"','"+t.Sex+"')";
            if (DataAccess.ExecSql(sql) > 0)
                return "success";
            else return "fail";
        }

        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <param name="t">学生对象</param>
        /// <returns>成功：success 失败：fail</returns>
        public static string EditStu(Student t)
        {
            string sql = "update tblStu set NO='" + t.NO + "',Name='" + t.Name + "',Sex='" + t.Sex + "' where ID='"+ t.ID +"'";
            if (DataAccess.ExecSql(sql) > 0)
                return "success";
            else return "fail";
        }
    }
}
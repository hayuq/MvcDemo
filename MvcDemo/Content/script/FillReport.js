﻿var toolbar;
$(function () {
    toolbar = new Toolbar({
        renderTo: 'toolbar',
        items: [{
            type: 'button',
            text: '保存',
            bodyStyle: 'save',
            useable: 'T',
            handler: function () {
                if (!toolbar.getDisabled(0))
                    DoSave();
            }
        }, '-', {
            type: 'button',
            text: '提交审核',
            bodyStyle: 'audit',
            useable: 'T',
            handler: function () {
                if (!toolbar.getDisabled(1))
                    DoAudit();
            }
        }, '-', {
            type: 'button',
            text: '返回',
            bodyStyle: 'back',
            useable: 'T',
            handler: function () {
                ajaxLoading();
                window.location = "../DailyReport/Fill_Report_Index";
            }
        }]
    });
    toolbar.render();
    Page_Init();
    $('#jh').combobox({
        onChange: function (newValue, oldValue) {
            $.post("../Common/LoadZYQByJH", { jh: newValue }, function (rsp) {
                $("#zyq").html(rsp);
            });
        }
    });

    //测试结果不合格，清空问题类型
    $("#csjg").combobox({
        onChange: function (newValue, oldValue) {
            if (newValue == "0")
                $("#wtlx").combotree('setValue', "");
        }
    });

    $('#cszt').combobox({
        onChange: function (newValue, oldValue) {
            if (newValue == "1")
                $("#csjg").combobox("setValue", "");
            $("select[id*='csjg_']").each(function () {
                if (newValue == "1")//过程中
                    $(this).combobox("setValue", "");
            });
        }
    });

    $("#jh").combobox({
        onChange: function (newValue, oldValue) {
            if (newValue == null || newValue == "") {
                return;
            }
            $.post("../Common/LoadZYQByJH", { jh: newValue }, function (rsp) {
                $("#zyq").html(rsp);
            });
            ReloadPageData();
        }
    });

    $("#csrq").datebox({
        onChange: function (newValue, oldValue) {
            ReloadPageData();
        }
    });

    InitScrollImage();
});

var grayRenderer = function (instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.backgroundColor = '#f1f1f1';
};

var rowNumberRender = function (instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.backgroundColor = '#f1f1f1';
    td.innerText = parseInt(row) + 1;
    td.innerHTML = parseInt(row) + 1;
    td.align = "center"
}

function Page_Init() {
    ajaxLoading();
    $.post("../DailyReport/GetLayerTestData", { rbid: $("#rbid").val() }, function (rsp) {
        LoadData(rsp);
    });
}

function LoadData(rsp) {

    toolbar.setDisabled(1, true);

    $("input[id*='btnClear_']").each(function () {
        $(this).removeAttr("disabled");
    });

    if ($("#rbid").val() != "") {
        $('#jh').combobox('disable');
        $('#csrq').datebox('disable');
       
        toolbar.setDisabled(1, false);
    }

    $('#jh').combobox('setValue', rsp["info"].jh);

    $('#gzlx').combobox('setValue', rsp["info"].gzlx);
    $('#csrq').datebox('setValue', rsp["info"].csrq);
    $('#csmd').combobox('setValue', rsp["info"].csmd);
    $('#cszt').combobox('setValue', rsp["info"].cszt);
    $('#csjg').combobox('setValue', rsp["info"].csjg);
    $('#wtlx').combotree('setValue', rsp["info"].wtlx);
    $('#csbz').combobox('setValue', rsp["info"].csbz);
    $('#gchwtms').val(rsp["info"].gchwtms);

    ///管柱图
    if (rsp["info"].gztlj)
        $(".viewer").zoomer({ source: rsp["info"].gztlj, controls: {
            position: 'top'
        }
        });
    $("#txtWJMC").val(rsp["info"].gztmc);
    $("#txtWJLJ").val(rsp["info"].gztlj);
    $("#uploadFile").val("");

   
    //附件
    $("#content_list").html("");
    $.each(rsp["info"].attachment, function (index, item) {
        var info = item.split('|');
        var html = "<dl style='width:425px;' id='dl_" + info[3] + "'>";
        html += "<dd>";
        html += "<table border='0' cellpadding='0' cellspacing='0'>";
        html += "<tr>";
        html += "<td height='50' width='60'>附件名称：</td>";
        html += "<td align='left'><a href='" + info[2] + "' target='_blank'>" + info[0] + "</a></td>";
        html += "<td width='50'><input type='hidden' name='attachment' value='" + item + "'/><a href='#'  class='easyui-linkbutton' iconCls='icon-remove' onclick='DeleteFile(\"" + info[3] + "\",\"" + info[0] + "\",\"" + info[2] + "\")'>删除</a></td>";
        html += "</tr>";
        html += "<tr>";
        html += "<td colspan='3'>";
        info[2] = info[2].toLowerCase();
        if (info[2].indexOf("jpg") > 0 || info[2].indexOf("bmp") > 0 || info[2].indexOf("png") > 0 || info[2].indexOf("jpeg") > 0) {
            html += "<img style='width:425px;' src='" + info[2] + "'/>";
        } else {
            html += "无法预览";
        }
        html += "</td>";
        html += "</tr>";
        html += "</table>";
        html += "</dd>";
        html += "</dl>";
        $("#content_list").append(html);
    })

    $("#uploadAttachment").val("");

   
    var qj_container = $("#qj");
    $("#qzsd").numberbox("setValue", rsp["qj"].qzsd);
    $("#qjpz").numberbox("setValue", rsp["qj"].qjpz);
    $("#qjsl1").numberbox("setValue", rsp["qj"].qjsl1);
    $("#qjsl2").numberbox("setValue", rsp["qj"].qjsl2);
    $("#qjyl1").numberbox("setValue", rsp["qj"].qjyl1);
    $("#qjyl2").numberbox("setValue", rsp["qj"].qjyl2);
    qj_container.handsontable({
        data: rsp["qj"].testData,
        colHeaders: true,
        height: 150,
        minSpareRows: 1,
        columns: [
                        { data: 'xh', width: 60, type: 'numeric', readOnly: true, renderer: rowNumberRender },
                        { data: 'yy', width: 120, type: 'numeric', format: '0.00' },
                        { data: 'ty', width: 120, type: 'numeric', format: '0.00' },
                        { data: 'rzl', width: 120, type: 'numeric', format: '0.00' },
                        { data: 'cz1', width: 120, readOnly: true, renderer: grayRenderer, format: '0.00' },
                        { data: 'cz2', width: 120, readOnly: true, renderer: grayRenderer, format: '0.00' }
                        ],
        colHeaders: ['全井测试', '油压(MPa)', '套压(MPa)', '日注量(m<sup>3</sup>/d)', '差值(m<sup>3</sup>/d)', '差值(%)'],
        afterChange: function (changes, source) {
            if (!changes) {
                return;
            }
            var instance = this;
            changes.forEach(function (change) {
                var row = change[0];
                var col = change[1];
                if (col == 'rzl') {
                    var qjpz = $("#qjpz").val();
                    var rzl = instance.getDataAtCell(row, 3);
                    var dcpz = (dcpz == "" || !dcpz) ? 0 : dcpz;
                    rzl = (rzl == "" || !rzl) ? 0 : rzl;
                    var t = ((qjpz == 0 || qjpz == "") ? 0 : 100*(rzl - qjpz) / qjpz);
                    instance.setDataAtCell(row, 4, (rzl - qjpz).toFixed(2));
                    instance.setDataAtCell(row, 5, t.toFixed(2));
                }
            });
        }
    });


    var $fcxx_container;
    $("input[id*='psq_']").each(function () {
        if (!rsp["fc"][$(this).val()]) return;
        $("#psqsd_" + rsp["fc"][$(this).val()].psq + "_").numberbox("setValue", rsp["fc"][$(this).val()].psqsd);
        $("#pzcd_" + rsp["fc"][$(this).val()].psq + "_").numberbox("setValue", rsp["fc"][$(this).val()].pzcd);
        $("#csqszlx_" + rsp["fc"][$(this).val()].psq + "_").combobox('setValue', rsp["fc"][$(this).val()].csqszlx);
        $("#csqszzt_" + rsp["fc"][$(this).val()].psq + "_").combobox('setValue', rsp["fc"][$(this).val()].csqszzt);
        $("#cshszlx_" + rsp["fc"][$(this).val()].psq + "_").combobox('setValue', rsp["fc"][$(this).val()].cshszlx);
        $("#cshszzt_" + rsp["fc"][$(this).val()].psq + "_").combobox('setValue', rsp["fc"][$(this).val()].cshszzt);
        $("#sfghsz_" + rsp["fc"][$(this).val()].psq + "_").combobox('setValue', rsp["fc"][$(this).val()].sfghsz);
        $("#cdxz_" + rsp["fc"][$(this).val()].psq + "_").combobox('setValue', rsp["fc"][$(this).val()].cdxz);
        $("#dcpz_" + rsp["fc"][$(this).val()].psq + "_").numberbox("setValue", rsp["fc"][$(this).val()].dcpz);
        $("#zssl1_" + rsp["fc"][$(this).val()].psq + "_").numberbox("setValue", rsp["fc"][$(this).val()].zssl1);
        $("#zssl2_" + rsp["fc"][$(this).val()].psq + "_").numberbox("setValue", rsp["fc"][$(this).val()].zssl2);
        $("#zsyl1_" + rsp["fc"][$(this).val()].psq + "_").numberbox("setValue", rsp["fc"][$(this).val()].zsyl1);
        $("#zsyl2_" + rsp["fc"][$(this).val()].psq + "_").numberbox("setValue", rsp["fc"][$(this).val()].zsyl2);
        $("#csjg_" + rsp["fc"][$(this).val()].psq + "_").combobox('setValue', rsp["fc"][$(this).val()].csjg);

        $fcxx_container = $("#fcxx_" + $(this).attr("id"));
        var psq = rsp["fc"][$(this).val()].psq;
//        $("#btnClear_" + psq).click(function () {
//            ClearData(this, $fcxx_container, psq);
//        });
        $fcxx_container.handsontable({
            data: rsp["fc"][$(this).val()].testData,
            minSpareRows: 1,
            height: 150,
            columns: [
                        { data: 'xh', width: 60, type: 'numeric', readOnly: true, renderer: rowNumberRender },
                        { data: 'yy', width: 120, type: 'numeric', className: 'htMiddle', format: '0.00' },
                        { data: 'ty', width: 120, type: 'numeric', format: '0.00' },
                        { data: 'rzl', width: 120, type: 'numeric', format: '0.00' },
                        { data: 'cz1', width: 120, readOnly: true, renderer: grayRenderer, format: '0.00' },
                        { data: 'cz2', width: 120, readOnly: true, renderer: grayRenderer, format: '0.00' }
                        ],
            colHeaders: ['分层测试', '油压(MPa)', '套压(MPa)', '日注量(m<sup>3</sup>/d)', '差值(m<sup>3</sup>/d)', '差值(%)'],
            afterChange: function (changes, source) {
                if (!changes) {
                    return;
                }
                var instance = this;
                changes.forEach(function (change) {
                    var row = change[0];
                    var col = change[1];
                    if (col == 'rzl') {
                        var dcpz = $("#dcpz_" + psq + "_").val();
                        var rzl = instance.getDataAtCell(row, 3);
                        var dcpz = (dcpz == "" || !dcpz) ? 0 : dcpz;
                        rzl = (rzl == "" || !rzl) ? 0 : rzl;
                        var t = (dcpz == 0 ? 0 : 100 * (rzl - dcpz) / dcpz);
                        instance.setDataAtCell(row, 4, (rzl - dcpz).toFixed(2));
                        instance.setDataAtCell(row, 5, t.toFixed(2));
                    }
                });
            }
        });
    });
    
    ajaxLoadEnd();
}

function InitScrollImage() {
    var page = 1;
    var i = 2; //每版放2个图片

    //向后 按钮
    $("span.next").click(function () {    //绑定click事件
        var content = $("div#content");
        var content_list = $("div#content_list");
        var v_width = content.width();
        var len = content.find("dl").length;
        var page_count = Math.ceil(len / i);   //只要不是整数，就往大的方向取最小的整数
        if (!content_list.is(":animated")) {    //判断“内容展示区域”是否正在处于动画
            if (page == page_count) {  //已经到最后一个版面了,如果再向后，必须跳转到第一个版面。
                return;
            } else {
                content_list.animate({ left: '-=' + v_width }, "slow");  //通过改变left值，达到每次换一个版面
                page++;
            }
        }
    });

    //往前 按钮
    $("span.prev").click(function () {
        var content = $("div#content");
        var content_list = $("div#content_list");
        var v_width = content.width();
        var len = content.find("dl").length;
        var page_count = Math.ceil(len / i);   //只要不是整数，就往大的方向取最小的整数
        if (!content_list.is(":animated")) {    //判断“内容展示区域”是否正在处于动画
            if (page == 1) {  //已经到第一个版面了,如果再向前，必须跳转到最后一个版面。
                return;
            } else {
                content_list.animate({ left: '+=' + v_width }, "slow");
                page--;
            }
        }
    });

}

function CheckNull(val) {
    if (val.trim() == "" || !val) {
        return true;
    }
    return false;
}

//检查偏水器的信息输入完整性
function CheckPSQ_IsValid(code) {
    if (!CheckNull($("#psqsd_" + code + "_").val()) ||
        !CheckNull($("#csqszlx_" + code + "_").combobox('getValue')) ||
        !CheckNull($("#csqszzt_" + code + "_").combobox('getValue')) ||
        !CheckNull($("#cshszlx_" + code + "_").combobox('getValue')) ||
        !CheckNull($("#cshszzt_" + code + "_").combobox('getValue')) ||
        !CheckNull($("#sfghsz_" + code + "_").combobox('getValue')) ||
        !CheckNull($("#cdxz_" + code + "_").combobox('getValue')) ||
        !CheckNull($("#dcpz_" + code + "_").val()) ||
        !CheckNull($("#zssl1_" + code + "_").val()) ||
        !CheckNull($("#zssl2_" + code + "_").val()) ||
        !CheckNull($("#zsyl1_" + code + "_").val()) ||
        !CheckNull($("#zsyl2_" + code + "_").val())) {
        if (CheckNull($("#csjg_" + code + "_").combobox('getValue'))) {
            return false;
        }
        return true;
    }
    return true;
}

function CheckPSQIsNull(code) {
    if (CheckNull($("#psqsd_" + code + "_").val()) &&
        CheckNull($("#csqszlx_" + code + "_").combobox('getValue')) &&
        CheckNull($("#csqszzt_" + code + "_").combobox('getValue')) &&
        CheckNull($("#cshszlx_" + code + "_").combobox('getValue')) &&
        CheckNull($("#cshszzt_" + code + "_").combobox('getValue')) &&
        CheckNull($("#sfghsz_" + code + "_").combobox('getValue')) &&
        CheckNull($("#cdxz_" + code + "_").combobox('getValue')) &&
        CheckNull($("#dcpz_" + code + "_").val()) &&
        CheckNull($("#zssl1_" + code + "_").val()) &&
        CheckNull($("#zssl2_" + code + "_").val()) &&
        CheckNull($("#zsyl1_" + code + "_").val()) &&
        CheckNull($("#csjg_" + code + "_").combobox('getValue'))&&
        CheckNull($("#zsyl2_" + code + "_").val())) {  
        return true;
    }
    return false;
}

function DoSave() {
    var layerData = [];
    var attachment = [];
    var psq_config;
    
    if ($('#jh').combobox('getValue') == "") {
        $.messager.alert('系统提示', '井号不能为空！', 'warning');
        return;
    }

    if ($('#csrq').datebox('getValue') == "") {
        $.messager.alert('系统提示', '测试日期不能为空！', 'warning');
        return;
    }

    if ($('#cszt').combobox('getValue') == "") {
        $.messager.alert('系统提示', '测试状态不能为空！', 'warning');
        return;
    }

    var check_null_passed = true;
    $("input[name='psqconfig']").each(function () {

        if (!check_null_passed) {
            return;
        }

        if ($('#cszt').combobox('getValue') == "0" || $('#cszt').combobox('getText') == "已完成") {
            if (!CheckPSQ_IsValid($(this).val())) {
                $.messager.alert('系统提示', '请选择配水器“' + $("#psqmc_" + $(this).val() + "_").val() + '”的测试结果！', 'warning');
                check_null_passed = false;
                return;
            }
        }

        var $fcxx_container = $("#fcxx_psq_" + $(this).val() + "_");
        var sjlrzt = 1;
        if (CheckPSQIsNull($(this).val()) && $fcxx_container.handsontable('getData').length == 1) {
            sjlrzt = 0;
        }
        var psq_model = {
            psq: $("#psq_" + $(this).val() + "_").val(),
            psqsd: $("#psqsd_" + $(this).val() + "_").val(),
            pzcd: $("#pzcd_" + $(this).val() + "_").val(),
            csqszlx: $("#csqszlx_" + $(this).val() + "_").combobox('getValue'),
            csqszzt: $("#csqszzt_" + $(this).val() + "_").combobox('getValue'),
            cshszlx: $("#cshszlx_" + $(this).val() + "_").combobox('getValue'),
            cshszzt: $("#cshszzt_" + $(this).val() + "_").combobox('getValue'),
            sfghsz: $("#sfghsz_" + $(this).val() + "_").combobox('getValue'),
            cdxz: $("#cdxz_" + $(this).val() + "_").combobox('getValue'),
            sjlrzt:sjlrzt,
            dcpz: $("#dcpz_" + $(this).val() + "_").val(),
            zssl1: $("#zssl1_" + $(this).val() + "_").val(),
            zssl2: $("#zssl2_" + $(this).val() + "_").val(),
            zsyl1: $("#zsyl1_" + $(this).val() + "_").val(),
            zsyl2: $("#zsyl2_" + $(this).val() + "_").val(),
            csjg: $("#csjg_" + $(this).val() + "_").combobox('getValue'),
            testData: $fcxx_container.handsontable('getData')
        };
        layerData.push(psq_model);
    });

    //输入合法性检查
    if (!check_null_passed) {
        return;
    }

    $("input[name='attachment']").each(function (index) {
        attachment.push($(this).val());
    });

    var $fcxx_container = $("#qj");
    var qj_model = {
        rbid: $("#rbid").val(),
        qzsd: $("#qzsd").val(),
        qjpz: $("#qjpz").val(),
        qjsl1: $("#qjsl1").val(),
        qjsl2: $("#qjsl2").val(),
        qjyl1: $("#qjyl1").val(),
        qjyl2: $("#qjyl2").val(),
        testData: $fcxx_container.handsontable('getData')
    }

    var model = {
        rbid: $("#rbid").val(),
        bm: $("#bm").val(),
        jh: $('#jh').combobox('getValue'),
        gzlx: $('#gzlx').combobox('getValue'),
        csrq: $('#csrq').datebox('getValue'),
        csmd: $('#csmd').combobox('getValue'),
        cszt: $('#cszt').combobox('getValue'),
        csjg: $('#csjg').combobox('getValue'),
        wtlx: $('#wtlx').combotree('getValue'),
        csbz: $('#csbz').combobox('getValue'),
        gchwtms: $('#gchwtms').val(),
        layerData: layerData,
        gztmc: $('#txtWJMC').val(),
        gztlj: $('#txtWJLJ').val(),
        wellData: qj_model,
        attachment: attachment
    };

    toolbar.setDisabled(0, true);
    $.post("../DailyReport/SaveDailyReport", { data: JSON.stringify(model), flag: "add" }, function (rsp) {
        if (rsp.code == "0") {
            $.messager.alert('系统消息', '请先提交审核“' + $('#jh').combobox('getValue') + '”在' + $('#csrq').datebox('getValue') + '的日报数据！', 'info');
        }
        else if (rsp.code == "1") {
            $.messager.alert('系统消息', '日报信息已经成功保存！', 'info');
            $("#rbid").val(rsp.data);
            toolbar.setDisabled(1, false);
        } else {
            $.messager.alert('系统消息', "保存失败，错误详细信息：" + rsp.message, 'info');
        }
        toolbar.setDisabled(0, false);
    });
}

var audit_flag = "0";
function DoAudit() {
    if (audit_flag == "1") { audit_flag = "0"; return; }
    if ($("#rbid").val() == "") {
        $.messager.alert('系统提示', '日报信息还未保存，不能提交审核！', 'info');
        audit_flag = "1";
        return;
    }

    if ($('#cszt').combobox('getValue')=='0') {//测试状态为已完成
        if (CheckNull($('#csjg').combobox('getValue'))) {
            $.messager.alert('系统提示', '请设置测试结果，并保存后提交审核！', 'warning');
            audit_flag = "1";
            toolbar.setDisabled(1, true);
            return;
        }

        var check_null_passed = true;
        $("input[name='psqconfig']").each(function () {
            if (!check_null_passed) {
                audit_flag = "1";
                toolbar.setDisabled(1, true);
                return;
            }
            if (!CheckPSQ_IsValid($(this).val())) {
                $.messager.alert('系统提示', '请设置配水器“' + $("#psqmc_" + $(this).val() + "_").val() + '”的测试结果，请保存后提交审核！', 'warning');
                check_null_passed = false;
                audit_flag = "1";
                toolbar.setDisabled(1, true);
                return;
            }
        });

        if (!check_null_passed) return;
    }

    

    toolbar.setDisabled(1, true);
    ajaxLoading();
    $.messager.confirm('确认对话框', '日报数据一旦提交审核后，录入人员就不可更改其内容。确定要提交审核？', function (r) {
        if (r) {
            $.post("../DailyReport/ToAudit", { rbid: $("#rbid").val(), status: '2', cszt: $('#cszt').combobox('getValue'), menuid: $('#menuid').val() }, function (rsp) {
                if (rsp == "success") {
                    $('#btnUpload').linkbutton('disable');
                    $('#btnUploadAttachment').linkbutton('disable');
                    toolbar.setDisabled(0, true);
                    $("input[id*='btnClear_']").each(function () {
                        $(this).attr("disabled", "disabled");
                    });
                    $("#btnClearQJ").attr("disabled", "disabled");
                    $(".fileremovecss").attr("disabled", "disabled");
                    ajaxLoadEnd();
                }
            });
        } else {
            $("input[id*='btnClear_']").each(function () {
                $(this).removeAttr("disabled");
            });
            toolbar.setDisabled(1, false);
            toolbar.setDisabled(0, false);
            $(".fileremovecss").removeAttr("disabled");
            ajaxLoadEnd();
        }
        audit_flag = "0";
    });
}

//添加附件
function upload() {
    var ext = '.jpg.jpeg.gif.bmp.png.';
    var f = $("#uploadFile").val();
    f = f.substr(f.lastIndexOf('.') + 1).toLowerCase();
    if (ext.indexOf('.' + f + '.') == -1) {
        $.messager.alert("提示", "图片格式不正确！", "warning");
        return false;
    }

    var input1 = $("<input type='hidden' name='rbid1' id='rbid1'/>");
    input1.attr('value', $("#rbid").val());
    $("#frmGZT").append(input1);
    var input2 = $("<input type='hidden' name='filetype' id='filetype'/>");
    input2.attr('value', "gzt");
    $("#frmGZT").append(input2);

    $("[id$='frmGZT']").ajaxSubmit({
        url: "../Document/upload",
        type: "post",
        dataType: "text",
        resetForm: false,
        success: function (msg) {
            if (msg == "empty") {
                $.messager.alert("提示", "禁止上传0KB大小的文件！", "warning");
            } else if (msg == "select") {
                $.messager.alert("提示", "请选择要上传的文件！", "warning");
            } else if (msg == "fail") {
                $.messager.alert("提示", "上传失败！", "warning");
            } else {
                var info = msg.split('|');
                if ($("#txtWJ").val() == "") {
                    $("#txtWJ").val(info[0]);
                } else
                    $("#txtWJ").val($("#txtWJ").val() + "|" + info[0]);
                var filename = $("#uploadFile").val().split("\\");
                
                $(".viewer").zoomer({ source: info[2], controls: {
                    position: 'top'
                }
                });
            $(".zoomer-image").attr("src", info[2]);
                $("#txtWJMC").val(info[0]);
                $("#txtWJLJ").val(info[2]);
                $("#uploadFile").val("");
            }
            return false;
        },
        error: function (jqXHR, errorMsg, errorThrown) {
            $.messager.alert("提示", "错误信息：" + errorMsg, "error");
            return false;
        }
    });

    $("#rbid1").remove();
    $("#filetype").remove();
}

function UploadAttachment() {
    var fileExist = false;
    $("input[name='attachment']").each(function (index) {
        var f = $("#uploadAttachment").val();
        f = f.substr(f.lastIndexOf('\\') + 1);
        var info = $(this).val().split('|');
        if (f == info[0]) {
            fileExist = true;
            return;
        }
    });

    if (fileExist) {
        $.messager.alert("提示", "文件已上传，请选择其他附件！", "warning");
        return;
    }

    var input1 = $("<input type='hidden' name='rbid1' id='rbid1'/>")
    input1.attr('value', $("#rbid").val());
    $("#frmAttachment").append(input1);

    var input2 = $("<input type='hidden' name='filetype' id='filetype'/>");
    input2.attr('value', "fj");
    $("#frmAttachment").append(input2);

    $("[id$='frmAttachment']").ajaxSubmit({
        url: "../Document/upload",
        type: "post",
        dataType: "text",
        resetForm: false,
        success: function (msg) {
            if (msg == "empty") {
                $.messager.alert("提示", "禁止上传0KB大小的文件！", "warning");
            } else if (msg == "select") {
                $.messager.alert("提示", "请选择要上传的文件！", "warning");
            } else if (msg == "fail") {
                $.messager.alert("提示", "上传失败！", "warning");
            } else {
                var info = msg.split('|');
                var html = "<dl style='width:425px;' id='dl_" + info[3] + "'>";
                html += "<dd>";
                html += "<table border='0' cellpadding='0' cellspacing='0'>";
                html += "<tr>";
                html += "<td height='50' width='60'>附件名称：</td>";
                html += "<td align='left'><a href='" + info[2] + "' target='_blank'>" + info[0] + "</a></td>";
                html += "<td width='50'><input type='hidden' name='attachment' value='" + msg + "'/><a href='#'  class='easyui-linkbutton fileremovecss' iconCls='icon-remove' onclick='DeleteFile(\"" + info[3] + "\",\"" + info[0] + "\",\"" + info[2] + "\")'>删除</a></td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td colspan='3'>";
                var ext = info[2].toLowerCase();
                if (ext.indexOf("jpg") > 0 || ext.indexOf("bmp") > 0 || ext.indexOf("png") > 0 || ext.indexOf("jpeg") > 0) {
                    html += "<img style='width:425px;' src='" + info[2] + "'/>";
                } else {
                    html += "无法预览";
                }
                html += "</td>";
                html += "</tr>";
                html += "</table>";
                html += "</dd>";
                html += "</dl>";
                $("#content_list").prepend(html);
                $("#uploadAttachment").val("");
            }
            $("#rbid1").remove();
            $("#filetype").remove();
            return false;
        },
        error: function (jqXHR, errorMsg, errorThrown) {
            $.messager.alert("提示", "错误信息：" + errorMsg, "error");
            return false;
        }
    });

    $("#rbid1").remove();
    $("#filetype").remove();
}

//全井数据--清除数据
function ClearQJ() {
    $.messager.confirm('确认', '确定要清除当前井的全井数据？', function (r) {
        if (r) {
            $("#qzsd").numberbox("reset");;
            $("#qjpz").numberbox("reset"); ;
            $("#qjsl1").numberbox("reset"); ;
            $("#qjsl2").numberbox("reset"); ;
            $("#qjyl1").numberbox("reset"); ;
            $("#qjyl2").numberbox("reset"); ;
            var empty = [];
            $("#qj").handsontable("loadData", empty);
        }
    });
}

function ClearDataFCXX(obj, psq) {
    $fcxx_container = $("#fcxx_psq_" + psq+"_");
    $.messager.confirm('确认', '确定要清除当前所选择的分层数据？', function (r) {
        if (r) {

            $("#psqsd_" + psq + "_").numberbox("reset");
            $("#csqszlx_" + psq + "_").combobox('setValue', '');
            $("#csqszzt_" + psq + "_").combobox('setValue', '');
            $("#cshszlx_" + psq + "_").combobox('setValue', '');
            $("#cshszzt_" + psq + "_").combobox('setValue', '');
            $("#sfghsz_" + psq + "_").combobox('setValue', '');
            $("#cdxz_" + psq + "_").combobox('setValue', '');
            $("#dcpz_" + psq + "_").numberbox("reset");
            $("#zssl1_" + psq + "_").numberbox("reset");
            $("#zssl2_" + psq + "_").numberbox("reset");
            $("#zsyl1_" + psq + "_").numberbox("reset");
            $("#zsyl2_" + psq + "_").numberbox("reset");
            $("#csjg_" + psq + "_").combobox('setValue', '');
            var empty = [];
            $fcxx_container.handsontable("loadData", empty);
        }
    });
}

//分层数据--清除数据
function ClearData(btn, obj, psq) {
    $.messager.confirm('确认', '确定要清除当前所选择的分层数据？', function (r) {
        if (r) {
            
            $("#psqsd_" + psq + "_").numberbox("reset");
            $("#csqszlx_" + psq + "_").combobox('setValue', '');
            $("#csqszzt_" + psq + "_").combobox('setValue', '');
            $("#cshszlx_" + psq + "_").combobox('setValue', '');
            $("#cshszzt_" + psq + "_").combobox('setValue', '');
            $("#sfghsz_" + psq + "_").combobox('setValue', '');
            $("#cdxz_" + psq + "_").combobox('setValue', '');
            $("#dcpz_" + psq + "_").numberbox("reset");
            $("#zssl1_" + psq + "_").numberbox("reset");
            $("#zssl2_" + psq + "_").numberbox("reset");
            $("#zsyl1_" + psq + "_").numberbox("reset");
            $("#zsyl2_" + psq + "_").numberbox("reset");
            $("#csjg_" + psq + "_").combobox('setValue', '');
            var empty = [];
            obj.handsontable("loadData", empty);
        }
    });
}

function DeleteFile(id, name, path) {
    $.messager.confirm('确认', '确定要删除附件“' + name + '”吗？', function (r) {
        if (r) {
            $("#dl_" + id).remove();
            $.post("../Document/delete", { fjmc: name, fjlj: path, rbid: $("#rbid").val() }, function (rsp) {
                if (rsp == "success") {

                }
            });
        }
    });

}

//井号和测试日期变化，重新加载数据
function ReloadPageData() {
//    if (!CheckNull($('#jh').combobox('getValue')) && !CheckNull($('#csrq').datebox('getValue'))) {
//        ajaxLoading();
//        $.post("../DailyReport/GetLayerTestDataByCSRQ", { csrq: $('#csrq').datebox('getValue'), jh: $('#jh').combobox('getValue') }, function (rsp) {
//            LoadData(rsp);
//            ajaxLoadEnd();
//        }).error(function (msg) {
//            ajaxLoadEnd();
//        }); ;
//    }
}

function qj_onChange(newValue, oldValue) {
    var data = $("#qj").handsontable('getDataAtCol', 3);
    var qjpz = $('#qjpz').numberbox('getValue');
    if (qjpz == "" || !qjpz) qjpz = 0;
    if (!data) return;
    for (var i = 0; i < data.length; i++) {
        if (data[i]) {
            var t = (qjpz == 0 ? 0 : 100 * (data[i] - qjpz) / qjpz);
            $('#qj').handsontable('setDataAtCell', i, 4, (data[i] - qjpz).toFixed(2));
            $('#qj').handsontable('setDataAtCell', i, 5, t.toFixed(2));
        }
    }
}

function dcpz_onChange(newValue, oldValue) {
    var data = $("#fcxx_psq" + $(this).attr("id").replace("dcpz", "")).handsontable('getDataAtCol', 3);
    var dcpz = newValue;
    if (dcpz == "" || !dcpz) dcpz = 0;
    if (!data) return;
    for (var i = 0; i < data.length; i++) {
        if (data[i]) {
            var t = (dcpz == 0 ? 0 : 100 * (data[i] - dcpz) / dcpz);
            $("#fcxx_psq" + $(this).attr("id").replace("dcpz", "")).handsontable('setDataAtCell', i, 4, (data[i] - dcpz).toFixed(2));
            $("#fcxx_psq" + $(this).attr("id").replace("dcpz", "")).handsontable('setDataAtCell', i, 5, t.toFixed(2));
        }
    }
}
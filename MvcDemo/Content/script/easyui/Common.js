﻿

$(function () {

    //油井(上油井号)
    $("#txtJH").combobox({
        url: "../Common/LoadAllJH",
        mode: "remote",
        width: 90,
        panelHeight: "auto",
        valueField: "value",
        textField: "text"
    });

    $("#txtQK").combobox({
        url: "../Common/LoadQK?yt=",
        mode: "remote",
        width: 90,
        panelHeight: "auto",
        valueField: "value",
        textField: "text"
    });

    //油田
    $("#txtYT").combobox({
        url: "../Common/LoadYT",
        mode: "remote",
        width: 90,
        panelHeight: "auto",
        valueField: "value",
        textField: "text",
        onChange: function (newValue, oldValue) {
            //加载区块数据
            $("#txtQK").combobox({
                url: "../Common/LoadQK?yt=" + newValue,
                mode: "remote",
                width: 90,
                panelHeight: "auto",
                valueField: "value",
                textField: "text"
            });
            //$('#txtQK').panel('refresh', '../Common/LoadQK?yt=' + newValue);

        }
    });

    

    //作业区
    $("#txtZYQ").combobox({
        url: "../Common/LoadAllZYQ",
        mode: "remote",
        width: 90,
        panelHeight: "auto",
        valueField: "value",
        textField: "text"
    });


    $("INPUT[class='timechoosen']").click(function () {
        var value = $(this).val();
        var d = new Date().Format("yyyy-MM-dd");

        if (value == "0") {//今天
            $("#dtStart").datebox({ "disabled": true });
            $("#dtEnd").datebox({ "disabled": true });
            $("#dtStart").datebox("setValue", d);
            $("#dtEnd").datebox("setValue", d);
        } if (value == "1") {//一月
            $("#dtStart").datebox({ "disabled": true });
            var start = new Date().Minus(30).Format("yyyy-MM-dd");
            $("#dtStart").datebox("setValue", start);
            $("#dtEnd").datebox({ "disabled": true });
            var end = new Date().Format("yyyy-MM-dd");
            $("#dtEnd").datebox("setValue", end);

        } if (value == "2") {
            $("#dtStart").datebox("setValue", "")
            $("#dtEnd").datebox("setValue", "");
            $("#dtStart").datebox({ "disabled": false });
            $("#dtEnd").datebox({ "disabled": false });
        }

        if (value == "2") {
            $("#dtStart").datebox("setValue", "")
            $("#dtEnd").datebox("setValue", "");
            $("#dtStart").datebox({ "disabled": false });
            $("#dtEnd").datebox({ "disabled": false });
        }

        if (value == "4") {//一周
         
            $("#dtStart").datebox({ "disabled": true });
            var start = new Date().Minus(7).Format("yyyy-MM-dd");
            $("#dtStart").datebox("setValue", start);
            $("#dtEnd").datebox({ "disabled": true });
            var end = new Date().Format("yyyy-MM-dd");
            $("#dtEnd").datebox("setValue", end);
        }
        if (value == "4") {
            var start = new Date().Minus(360).Format("yyyy-MM");
            $("#dtStart").val(start);
            var end = new Date().Format("yyyy-MM");
            $("#dtEnd").val(end);
            $("#dtStart").attr("disabled", "disabled");
            $("#dtEnd").attr("disabled", "disabled");
        }
        if (value == "5") {
            var d = new Date().Format("yyyy-MM");
            $("#dtStart").val(d);
            $("#dtEnd").val(d);
            $("#dtStart").removeAttr("disabled");
            $("#dtEnd").removeAttr("disabled");
        }
        if (value == "6") {
            $("#dtStart").datebox({ "disabled": true });
            var start = new Date().Minus(7).Format("yyyy-MM-dd");
            $("#dtStart").datebox("setValue", start);
            $("#dtEnd").datebox({ "disabled": true });
            var end = new Date().Format("yyyy-MM-dd");
            $("#dtEnd").datebox("setValue", end);

        }
    });


});

Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
   
Date.prototype.Minus = function(days) {
    var interval = days*24*60*60*1000;
    this.setTime(this.getTime() - interval);
    return this;
}

function selectMonth() {  
    WdatePicker({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false });  
 } 


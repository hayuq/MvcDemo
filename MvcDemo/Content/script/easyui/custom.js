﻿/**
* EasyUI DataGrid根据字段动态合并单元格
* 参数 datagrid 要合并table的id
* 参数 columns 要合并的列,用逗号分隔(例如："name,department,office");
* 参数 idField 要合并的主列，可为空
* 参数 fieldColumns 根据主列字段合并的列,用逗号分隔,主列为空，该列无效
*/
function mergeCellsByField(datagrid, columns, idField, fieldColumns) {
    var ColArray = columns.split(",");
    var ColFieldArray;
    var dg = $("#" + datagrid);
    var TableRowCnts = dg.datagrid("getRows").length;
    var rowSpan;
    var PerTxt = "";
    var CurTxt = "";
    var alertStr = "";
    var idFieldStartRow;
    var rows = dg.datagrid("getRows");

    if (idField != undefined) {
        ColFieldArray = fieldColumns.split(",");
    }
    for (j = ColArray.length - 1; j >= 0; j--) {
        PerTxt = "";
        rowSpan = 1;

        for (i = 0; i <= TableRowCnts; i++) {
            if (i == TableRowCnts) {
                CurTxt = "";
            } else {
                CurTxt = rows[i][ColArray[j]];
            }

            if (PerTxt == CurTxt) {
                rowSpan += 1;
            } else {
                dg.datagrid("mergeCells", {
                    index: i - rowSpan,
                    field: ColArray[j],
                    rowspan: rowSpan,
                    colspan: null
                });
                dg.datagrid("mergeCells", {
                    index: i - rowSpan,
                    field: "Ideparture",
                    rowspan: rowSpan,
                    colspan: null
                });

                if (idField != undefined && idField != null
                    && idField == ColArray[j]) {
                    idFieldStartRow = i - rowSpan;
                    for (k = ColFieldArray.length - 1; k >= 0; k--) {
                        dg.datagrid("mergeCells", {
                            index: idFieldStartRow,
                            field: ColFieldArray[k],
                            rowspan: rowSpan,
                            colspan: null
                        });
                    }
                }
                rowSpan = 1;
            }
            PerTxt = CurTxt;
        }
    }
}


/**
* EasyUI DataGrid导出指定datagrid数据
* 参数 tableID 要合并table的id
* 参数 colList 要合并的列,用逗号分隔(例如："name,department,office");
*/
function onExportExcel(datagrid, filename, mergeColumns) {
    var content = ChangeToTable(datagrid, mergeColumns, true);
    if (content == "")
        return;

    var f = $('<form action="../Common/ExportExcel" method="post" id="export_form"></form>');
    var i = $('<input type="hidden" id="data" name="data" />');
    var l = $('<input type="hidden" id="filename" name="filename" />');
    i.val(content);
    i.appendTo(f);
    l.val(filename);
    l.appendTo(f);
    f.appendTo(document.body).submit();
}

//生成导出表格
function ChangeToTable(datagrid, mergeColumns, isExportData) {
    if (datagrid == undefined || datagrid == null || datagrid.length == 0) {
        $.messager.alert('提示', 'Datagrid控件错误！', 'warning');
        return "";
    }

    var dg = $("#" + datagrid);
    var strTable = '<table cellspacing="0" class="export_table">';
    var frozenColumns = dg.datagrid("options").frozenColumns;  // 得到frozenColumns对象
    var columns = dg.datagrid("options").columns;    // 得到columns对象
    var nameList = new Array();

    // 载入title
    if (typeof columns != 'undefined' && columns != '') {
        $(columns).each(function (index) {
            strTable += '<tr>';
            if (typeof frozenColumns != 'undefined' && typeof frozenColumns[index] != 'undefined') {
                for (var i = 0; i < frozenColumns[index].length; ++i) {
                    if (!frozenColumns[index][i].hidden) {
                        strTable += '<th width="' + frozenColumns[index][i].width + '"';
                        if (typeof frozenColumns[index][i].rowspan != 'undefined' && frozenColumns[index][i].rowspan > 1) {
                            strTable += ' rowspan="' + frozenColumns[index][i].rowspan + '"';
                        }
                        if (typeof frozenColumns[index][i].colspan != 'undefined' && frozenColumns[index][i].colspan > 1) {
                            strTable += ' colspan="' + frozenColumns[index][i].colspan + '"';
                        }
                        if (typeof frozenColumns[index][i].field != 'undefined' && frozenColumns[index][i].field != '') {
                            nameList.push(frozenColumns[index][i]);
                        }
                        strTable += '>' + frozenColumns[0][i].title + '</th>';
                    }
                }
            }
            for (var i = 0; i < columns[index].length; ++i) {
                if (!columns[index][i].hidden) {
                    strTable += '<th width="' + columns[index][i].width + '"';
                    if (typeof columns[index][i].rowspan != 'undefined' && columns[index][i].rowspan > 1) {
                        strTable += ' rowspan="' + columns[index][i].rowspan + '"';
                    }
                    if (typeof columns[index][i].colspan != 'undefined' && columns[index][i].colspan > 1) {
                        strTable += ' colspan="' + columns[index][i].colspan + '"';
                    }
                    if (typeof columns[index][i].field != 'undefined' && columns[index][i].field != '') {
                        nameList.push(columns[index][i]);
                    }
                    strTable += '>' + columns[index][i].title + '</th>';
                }
            }
            strTable += '</tr>';
        });
    }

    var ColArray = mergeColumns.split(",");

    // 载入内容
    var rows = dg.datagrid("getRows"); // 这段代码是获取当前页的所有行
    for (var i = 0; i < rows.length; ++i) {
        strTable += '<tr>';
        for (var j = 0; j < nameList.length; ++j) {
            //列名
            var e = nameList[j].field.lastIndexOf('_0');
            var nameField = "";
            if (e + 2 == nameList[j].field.length) {
                nameField = nameList[j].field.substring(0, e);
            }
            else {
                nameField = nameList[j].field;
            }

            var merge = false;
            for (var k = 0; k < ColArray.length; ++k) {
                if (ColArray[k] == nameField) {
                    merge = true;
                    break;
                }
            }

            //如果为合并单元格，直接跳过
            var strTD = rows[i][nameField];
            if (merge && i > 0 && strTD == rows[i - 1][nameField])
                continue;

            var rowspan = 1;
            if (merge) {
                for (var m = i + 1; m < rows.length; ++m) {
                    if (strTD == rows[m][nameField])
                        rowspan++;
                }
            }

            strTable += '<td';
            if (nameList[j].align != 'undefined' && nameList[j].align != '') {
                strTable += ' style="text-align:' + nameList[j].align + ';"';
            }
            if (rowspan > 1)
                strTable += ' rowspan="' + rowspan + '"';
            strTable += '>';
            if (isExportData || j < 3) {
                strTable += strTD;
            } else {
                strTable += "\t";
            }
            strTable += '</td>';
        }
        strTable += '</tr>';
    }
    strTable += '</table>';
    return strTable;
}

﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Index</title>
   <link rel="stylesheet" type="text/css" href="<%= Url.Content("~/Content/script/easyui/themes/default/easyui.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= Url.Content("~/Content/script/easyui/themes/icon.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= Url.Content("~/Content/style/web.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= Url.Content("~/Content/style/common.css") %>" />
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/jquery-1.11.1.min.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/jquery.easyui.min.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/jquery.jdirk.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/jquery.json-2.4.min.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/jquery.toolbar.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/easyui-lang-zh_CN.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/WdatePicker.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/easyui/json2.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/Common.js") %>"></script>
    <link rel="stylesheet" type="text/css" href="<%= Url.Content("~/Content/style/Toolbar.css") %>" />
    <script type="text/javascript" src="<%= Url.Content("~/Content/script/Toolbar.js") %>"></script>
</head>
<body>
    <div id="p" class="easyui-panel" title="学生信息管理"     
        style="padding:1px;background:#fafafa;"   
        data-options="fit:true">
         <div id="cc" class="easyui-layout" data-options="fit:true,border:false" >   
         <div data-options="region:'north',border:true" style="height:50px;margin-top:1px;">

      <table width="100%" style="height:40px">
                <tr>
                    <td width="50px" align="right">学号：</td>
                    <td width="130px">
                        <input id="txtXH" name="txtXH" type="text" class="easyui-validatebox"   
                            style="width:130px"/>
                    </td>

                    <td>
                        &emsp;<a id="btnQuery" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="DoQuery()">查询</a>
                          <%-- &emsp;<a id="A1" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-export'" onclick="DoExport()">导出</a>--%>
                    </td>
                </tr>
            </table>
     </div>
      <div data-options="region:'center',border:false">
      <table id="dg"></table>
     <div id="tb">
         <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="add()">添加</a>
       </div>
     </div>
      </div>

     <div id="dd" class="easyui-dialog" title="添加学生信息" > 
        <form id="ff" method="post" url="save"> 
            <table width="100%" cellspacing="10">
                <tr><td height="2px"></td></tr>
                <tr>
                    <td align="right">学号：</td>
                    <td>
                        <input class="easyui-validatebox" style="width:200px" name="txtNO" id="txtNO" maxlength="25"/> 
                   
                       <input name="action" id="action" type="hidden"/>
                       <input id="code" name="code" type="hidden"/>
                    </td>
                </tr>
                <tr>
                    <td align="right">姓名：</td>
                    <td>
                        <input class="easyui-validatebox" style="width:200px" name="txtName" id="txtName" maxlength="25"  /> 
                   
                    </td>
                </tr>
                  <tr>
                    <td align="right">性别：</td>
                    <td>
                        <input class="easyui-validatebox" style="width:200px" name="txtSex" id="txtSex" maxlength="25" /> 
                   
                    </td>
                </tr>
                 <tr>
                    <td colspan="2" align="center" height="40">
                    
                        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save()">保存</a>&emsp;
                        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="cancel()">取消</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    </div>
    <script type="text/javascript">
        $(function () {
           
            $('#dg').datagrid({
                fit: true, //表格撑满
                singleSelect: true, //允许选择单行
                pagination:true, //显示分页工具栏
                rownumbers: true,
                border: false,
                url: '../Student/GetALLStu', //指定调用StudentController的GetALLStu方法
                queryParams: { sXH: $("#txtXH").val() }, //查询参数列表
                columns: [[
                { field: 'ID', title: 'ID', width: 40, editor: "text", align: 'center' ,hidden:true },
                { field: 'NO', title: '学号', width: 120, editor: "text", align: 'center' },
                { field: 'Name', title: '姓名', width: 100, align: 'center' },
                { field: 'Sex', title: '性别', width: 52, editor: "text", align: 'center' },
                { field: 'CZ', title: '操作', width: 150, align: 'center',
                    formatter: function (value, row, index) {
                        var html = [];
                        html.push("<a id='view_" + index + "' href='#' onclick='EditStu(\"" + index + "\")'>修改</a>");
                        html.push("<a id='view_" + index + "' href='#' onclick='DeleteStu(\"" + index + "\")'>删除</a>"); 
                        return html.join("&nbsp;|&nbsp;");
                    }
                }

            ]],
               
//	            rowStyler: function(index,row){ //定义特定行的css样式
//		            if (row.NO % 2 == 0){
//			            //return 'background-color:#6293BB;color:#fff;'; // return inline style
//			            // the function can return predefined css class and inline style
//		                return { class: 'r1', style: 'background-color:#6293BB;color:#fff' };	
//	                }
//                },
                toolbar: '#tb'
            });

            $('#dd').dialog({
                title: '添加学生信息',
                width: 360,
                height: 200,
                closed: true,
                cache: false,
                modal: true
            });

        });

        //点击添加按钮后显示添加或修改界面
        function add() {
            $('#ff').form('clear');
            $("#dd").dialog("open");
            $("#action").val("add");
           
        }

       //查询
        function DoQuery() { //将学号文本框的值作为参数进行查询并显示到表格中
            $('#dg').datagrid('load', {
                sXH: $("#txtXH").val() //参数
            });
        }

        //编辑
        function EditStu(index) {
            $('#dd').dialog({ title: "修改学生信息" });
            var row = $('#dg').datagrid("selectRow", index).datagrid("getSelected");
            if (!row) {
                return;
            }

            $('#ff').form('clear'); //清除form之前显示的数据

            $("#action").val("edit");//将值存进input元素中
            $('#code').val(row.ID);
            $('#txtNO').val(row.NO);
            $('#txtName').val(row.Name);
            $('#txtSex').val(row.Sex);
            $("#dd").dialog("open");
        }

        //删除
        function DeleteStu(index) {
            var row = $('#dg').datagrid("selectRow", index).datagrid("getSelected");
            if (!row) {
                return;
            }

            $.messager.confirm('提示', '您确认想要删除学生：' + row.Name + "？", function (r) {
                if (r) {
                    $.post("../Student/DeleteStuById", { ID: row.ID }, function (rsp) {
                        if (rsp == "success") {
                            $('#dg').datagrid("load");
                        }
                        else {
                            $.messager.alert('提示', '删除失败，请重试!', 'warning');
                        }
                    });
                }
            });
        }

        //保存
        function save() {

            var NO = $('#txtNO').val();
            if ($.trim(NO) == "") {
                $.messager.alert('提示', '学号不能为空!', 'warning');
                return;
            }
            if(!/\d/.test(NO)) {
                $.messager.alert('提示', '学号只能为数字!', 'warning');
                return;
            }
            if ($.trim($('#txtName').val()) == "") {
                $.messager.alert('提示', '姓名不能为空!', 'warning');
                return;
            }
            if ($.trim($('#txtSex').val()) == "") {
                $.messager.alert('提示', '性别不能为空!', 'warning');
                return;
            }
            var model = { //将input的值作为参数传递到StudentController的特定方法中
                ID: $('#code').val(),
                NO: $('#txtNO').val(),
                Name: $('#txtName').val(),
                Sex: $('#txtSex').val()
            };

            $.messager.progress(); //显示进度条

            if ($("#action").val() == "add") { //添加
                $.post("../Student/AddStu", { data: JSON.stringify(model) },
                  function (rsp) {
                      $.messager.progress('close');  //停止显示进度条
                      if (rsp == "success") { //添加成功
                        
                        $("#dd").dialog("close");
                        $('#dg').datagrid("load"); //重新加载表格显示数据
                    }
                    
                    else if(rsp == "exist")
                        $.messager.alert('提示', '该学号已经存在!', 'warning');
                    else 
                        $.messager.alert('提示', '保存失败，请重试!', 'warning');
                });
            }
            else { //修改
                $.post("../Student/EditStu", { data: JSON.stringify(model) },
                  function (rsp) {
                      $.messager.progress('close');  //停止显示进度条
                      if (rsp == "success") { //修改成功
                          
                        $("#dd").dialog("close"); 
                        $('#dg').datagrid("load"); //重新加载表格显示数据
                    }
                    else { 
                        $.messager.alert('提示', '保存失败，请重试!', 'warning');
                    }
                });
            }
        }

        //关闭弹出窗口
        function cancel() {
            $("#dd").dialog("close");
        }
    </script>
</body>
</html>
